import { firebase, googleAuthProvider } from '../../firebase/fbConfig'
import { push } from 'connected-react-router'

export const emailSignIn = (credentials) => {
  return (dispatch, getState) => {    
    firebase.auth().signInWithEmailAndPassword(
      credentials.email,
      credentials.password
    ).then(() => {
      dispatch(push('/'))
    }).catch((err) => {
      dispatch({
        type: 'LOGIN_ERROR',
        err
      })
    })
  }
}

export const passwordReset = (emailAddress) => {
  var auth = firebase.auth();
    auth.sendPasswordResetEmail(emailAddress)
      .then(() => {
        console.log("Reset email sent")  
      }).catch((error) => {
    console.log('Error: '+error)
  });     
}

export const startGoogleLogin = () => {
  return (dispatch) => {
    return firebase.auth().signInWithPopup(googleAuthProvider).then(() => {
      dispatch(push('/'))
    })
  }  
}

export const login = (user = {}) => ({
  type: 'LOGIN',
  user
})

export const startLogout = () => {
  return (dispatch) => {    
    return firebase.auth().signOut().then(() => {
      dispatch(push('pages/login-page'))
    })
  }
}

export const logout = () => ({ type: 'LOGOUT' })