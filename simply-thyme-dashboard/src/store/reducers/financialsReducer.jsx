// OVERVIEW DATA
// Orders Data
const expenses = {
  data: {
    // For pie chart on dashboard
    labels: ["Salaries", "Office", "Fuel and Repairs", "Misc"],
    series: [10, 30, 20, 40]
  },
  options: {
    height: "310px",
    width: "auto",
    donut: true
  }
};

const estimates = {
  // for line chart for financials estimates
  data: {
    labels: ["NOV", "DEC", "JAN", "FEB", "MAR", "APR"],
    series: [
      [5, 9, 7, 8, 3, 10]
    ]
  },
  options: {
    height: "310px",
    low: 0,
    showArea: true
  }
}

const sales = {
  // Line Chart
  data: {
    labels: ["NOV", "DEC", "JAN", "FEB", "MAR", "APR"],
    series: [
      [2, 5, 4, 3, 4, 7]
    ]
  },
  options: {
    height: "310px",
    low: 0
  }
}

const tasks = {
  tableHead: [
    "TITLE",
    "DESCRIPTION",
    "DUE",
    "ACTIONS"
  ],
  tableData: [
    ["Fill out Payroll Forms", "Fill out Payroll Forms", "2 days overdue"],
    ["Fill out Payroll Forms", "Fill out Payroll Forms", "2 days overdue"],
  ]
}

//INVOICES PAGE
const invoices = {
  columns: [
    {
      Header: "CUSTOMER",
      accessor: "customer"
    },
    {
      Header: "COMPANY",
      accessor: "company"
    },
    {
      Header: "PHONE",
      accessor: "phone"
    },
    {
      Header: "EMAIL",
      accessor: "email"
    },
    {
      Header: "BALANCE",
      accessor: "balance"
    },
    {
      Header: "STATUS",
      accessor: "status"
    },
    {
      Header: "INVOICE DATE",
      accessor: "date"
    },
    {
      Header: "",
      accessor: "actions",
      sortable: false,
      filterable: false
    }
  ],
  dataRows: [
    ["Steven Ball", "Ball Aerospace", "555-555-5555", "sball@ballaero.com", "$1,067.59", "Unpaid", "30 days ago"],
    ["Steven Ball", "Ball Aerospace", "555-555-5555", "sball@ballaero.com", "$1,067.59", "Unpaid", "30 days ago"],
    ["Steven Ball", "Ball Aerospace", "555-555-5555", "sball@ballaero.com", "$1,067.59", "Unpaid", "15 days ago"],
    ["Steven Ball", "Ball Aerospace", "555-555-5555", "sball@ballaero.com", "$1,067.59", "Unpaid", "15 days ago"],
    ["Steven Hall", "Ball Aerospace", "555-555-5555", "sball@ballaero.com", "$1,067.59", "Unsent", "null"],
    ["Steven Hall", "Ball Aerospace", "555-555-5555", "sball@ballaero.com", "$1,067.59", "Unsent", "null"],
    ["Steven Hall", "Ball Aerospace", "555-555-5555", "sball@ballaero.com", "$1,067.59", "Unsent", "null"],
    ["Steven Hall", "Ball Aerospace", "555-555-5555", "sball@ballaero.com", "$1,067.59", "Unsent", "null"]
  ]
};

//EMPLOYEES PAGE
const overhead = {
  data: {
    labels: ["Office", "Drivers"],
    series: [
      [1000, 1500],
      [3000, 2000],
      [2000, 3000],
      [4000, 2000]
    ]
  },
  options: {
    stackBars: true,
    reverseData: true,
    horizontalBars: true
  }
}

const fuelCosts = {
  data: {
    labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN"],
    series: [[2500, 4000, 3700, 3100, 5500, 4200]]
  }
}

const initState = {
  expenses,
  estimates,
  invoices,
  tasks,
  sales,
  overhead,
  fuelCosts
}

export default (state = initState, action) => {
  switch (action.type) {
    default:
      return state
  }
}
