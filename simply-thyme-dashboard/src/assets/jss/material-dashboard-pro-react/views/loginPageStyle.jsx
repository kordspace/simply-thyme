// ##############################
// // // LoginPage view styles
// #############################

import {
  container,
  cardTitle,
  dangerColor,
  whiteColor
} from "assets/jss/material-dashboard-pro-react.jsx";

import { ltrButton } from "assets/jss/ltr-styles.jsx"

const loginPageStyle = theme => ({
  ltrButton: {
    ...ltrButton,
    marginBottom: '20px'
  },
  container: {
    ...container,
    zIndex: "4",
    [theme.breakpoints.down("sm")]: {
      paddingBottom: "100px"
    }
  },
  cardTitle: {
    ...cardTitle,
    fontSize: '24px'
  },
  textCenter: {
    textAlign: "center"    
  },
  textDanger: {
    fontSize: '12px',
    color: dangerColor,
    textAlign: "center"
  },
  justifyContentCenter: {
    justifyContent: "center !important"
  },
  customButtonClass: {
    "&,&:focus,&:hover": {
      color: "#FFFFFF"
    },
    marginLeft: "5px",
    marginRight: "5px"
  },
  inputAdornment: {
    marginRight: "18px"
  },
  inputAdornmentIcon: {
    color: whiteColor
  },
  cardHidden: {
    opacity: "0",
    transform: "translate3d(0, -60px, 0)"
  },
  cardHeader: {
    margin: "20px",
    padding: "10px 100px"
  },
  socialLine: {
    padding: "0.9375rem 0"
  }
});

export default loginPageStyle;
