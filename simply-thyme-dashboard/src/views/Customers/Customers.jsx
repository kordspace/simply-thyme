import React from "react";
import { connect } from 'react-redux';
import compose from 'recompose/compose';

// react component for creating dynamic tables
import ReactTable from "react-table";
import createTableObject from "components/Table/createReactTableObject.jsx"

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
import Assignment from "@material-ui/icons/Assignment";
import Dvr from "@material-ui/icons/Dvr";
import Favorite from "@material-ui/icons/Favorite";
import Close from "@material-ui/icons/Close";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import ArrowDownward from "@material-ui/icons/ArrowDownward";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardHeader from "components/Card/CardHeader.jsx";

import { cardTitle } from "assets/jss/material-dashboard-pro-react.jsx";
import Style from "assets/jss/material-dashboard-pro-react/views/customers/customersStyle";


class Orders extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openOrders: createTableObject(this.props.openOrders),      
      incompleteOrders: createTableObject(this.props.incompleteOrders) 
    };
  } 

  render() {    
    const { classes } = this.props;
    return (
      <GridContainer spacing={8}>
        <GridItem xs={6} sm={2}>
          <Card>
            <h2 className={classes.cardTitle}><ArrowUpward className={classes.successIcon} />07</h2>
            <p className={classes.cardSubtitle}>Pending Orders</p>
          </Card>
        </GridItem>
        <GridItem xs={6} sm={2}>
          <Card>
            <h2 className={classes.cardTitle}><ArrowDownward className={classes.dangerIcon} />32</h2>
            <p className={classes.cardSubtitle}>Orders in Transit</p>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={8}>
          <Card>
            <div className={classes.alignRight}><h1 className={classes.cardTitle}>$40,634/12,000,000</h1></div>
          </Card>
        </GridItem>
        <GridItem xs={12}>
          <h1 className={classes.title}>OPEN/ACTIVE ORDERS</h1>
        </GridItem>
        <GridItem xs={12}>
          <ReactTable
            data={this.state.openOrders}
            filterable
            columns={this.props.openOrders.columns}
            defaultPageSize={5}
            //showPaginationTop
            showPaginationBottom
            style={{
              color: "#f2f2f2"
            }}
            className="-striped -highlight"
          />
        </GridItem>
        <GridItem xs={12}>
          <h1 className={classes.title}>INCOMPLETE ORDERS</h1>
        </GridItem>
        <GridItem xs={12}>
          <ReactTable
            data={this.state.incompleteOrders}
            filterable
            columns={this.props.incompleteOrders.columns}
            defaultPageSize={5}
            //showPaginationTop
            showPaginationBottom
            style={{
              color: "#f2f2f2"
            }}
            className="-striped -highlight"
          />
        </GridItem>
      </GridContainer>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    openOrders: state.customers.openOrders,
    incompleteOrders: state.customers.incompleteOrders    
  }
}

export default compose(
  connect(mapStateToProps),
  withStyles(Style)
)(Orders);
