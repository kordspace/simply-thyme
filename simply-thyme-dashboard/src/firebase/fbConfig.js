import * as firebase from 'firebase'
import 'firebase/firestore' 
import 'firebase/auth' 

// Initialize Firebase
var config = {
  apiKey: "AIzaSyBwRv5QZX2rR60MTv8sSD6m547CtFizqMg",
  authDomain: "simply-thyme.firebaseapp.com",
  databaseURL: "https://simply-thyme.firebaseio.com",
  projectId: "simply-thyme",
  storageBucket: "simply-thyme.appspot.com",
  messagingSenderId: "31099096084"
};

firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true }); 
const googleAuthProvider = new firebase.auth.GoogleAuthProvider() 

export {firebase, googleAuthProvider, firebase as default};