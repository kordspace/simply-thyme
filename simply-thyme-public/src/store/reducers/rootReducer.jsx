import authReducer from './authReducer'
import formReducer from './formReducer'
import { trackingLocationDataReducer, trackingMapDataReducer } from './TrackingReducer'
import { combineReducers } from 'redux'
import { firestoreReducer } from 'redux-firestore'

const rootReducer = combineReducers({
    auth: authReducer,
    form: formReducer,    
    firestore: firestoreReducer,    
    trackingLocationData: trackingLocationDataReducer,
    trackingMapData: trackingMapDataReducer
});

export default rootReducer