import React from "react";
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import PropTypes from "prop-types";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";

// @material-ui/icons
import Face from "@material-ui/icons/Face";
import Email from "@material-ui/icons/Email";
// import LockOutline from "@material-ui/icons/LockOutline";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";

import DirectionsMap from "components/Map/DirectionsMap"
import { startTracking } from "store/actions/assetTrackingActions"

import styles from "assets/jss/material-kit-pro-react/components/forms/trackingFormStyle.jsx";

class Form extends React.Component {
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      billNum: '',
      billError: false
    };
  }

  handleChange = (e) => {
    this.setState({ [e.target.id]: e.target.value })
  }

  handleSubmit = (e) => {
    e.preventDefault();
    !!this.state.billNum ? (      
      this.props.startTracking(this.state.billNum)
    ) : (
        this.setState({
          billError: true
        })
      )
  }
  getMiles = (i) => {
    i = i * 0.000621371192
    return i.toFixed(2) + " miles"
  }

  render() {
    const { classes } = this.props;
    
    return (
      <GridContainer justify="center">
        <GridItem xs={11} s={10}>
          <br /> <br />
          <form
            style={{
              textAlign: "center"
            }}
            onSubmit={this.handleSubmit}
          >
            <GridContainer>
              <GridItem xs={9}>
                <CustomInput
                  labelText="Bill Number"
                  id="billNum"
                  formControlProps={{
                    fullWidth: true
                  }}
                  inputProps={{
                    type: "text",
                    onChange: (event) => this.handleChange(event)
                  }}
                />
              </GridItem>
              <GridItem xs={3}>
                <Button
                  className={classes.ltrButton}
                  type="submit"
                  rel="noopener noreferrer"
                >
                  SUBMIT
              </Button>
              </GridItem>
            </GridContainer>
          </form>
          {
            this.state.billError || !!this.props.trackingMapData.mapError ? (
              <p className={classes.errorText}>Please enter valid Bill Number</p>              
            ) : (null)
          }
          <h5 className={classes.bodyText1}> Enter your Bill Number above to track your shipment.You can find your Bill Number in your
          email receipt.You can also find your Bill Number in your order history when you log into your account.</h5>
          <br />
          {
            // Only render the map if the keep trucking query is successful
            !!this.props.trackingLocationData.truckNum && <GridContainer>
              <GridItem xs={12} sm={3}>
                {
                  this.props.trackingMapData.distance ? (
                    <div>
                      <h3 className={classes.header1}>SHIPMENT<br />INFORMATION</h3>
                      <p className={classes.bodyText1}>Bill Number: {this.state.billNum}</p>
                      <p className={classes.bodyText1}>Distance: {this.getMiles(this.props.trackingMapData.distance)}</p>
                      <p className={classes.bodyText1}>Start Location: {this.props.trackingMapData.routes[0].legs[0].start_address}</p>
                      <p className={classes.bodyText1}>Truck Location: {this.props.trackingMapData.routes[0].legs[1].start_address}</p>
                      <p className={classes.bodyText1}>Destination: {this.props.trackingMapData.routes[0].legs[1].end_address}</p>

                    </div>
                  ) : (
                      null
                    )
                }
              </GridItem>
              <GridItem xs={12} sm={9}>
                <DirectionsMap />
              </GridItem>
            </GridContainer>
          }
        </GridItem>
      </GridContainer>
    );
  }
}

Form.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapDispatchToProps = (dispatch) => ({
  startTracking: (billNum) => dispatch(startTracking(billNum))
})

const mapStateToProps = (state) => {
  return {
    trackingLocationData: state.trackingLocationData,
    trackingMapData: state.trackingMapData
  }
}

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Form);
