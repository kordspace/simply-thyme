import React from 'react';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";
import { darkMapStyles as mapStyles } from "./MapsStyles"

const RegularMap = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap
      defaultZoom={12}
      defaultCenter={{ lat: 39.784185, lng: -104.911220 }}
      defaultOptions={{
        scrollwheel: true,
        styles: mapStyles
      }}      
    >
      <Marker
        position={{ lat: 39.784185, lng: -104.911220 }}
        icon={{
            url: "/truck-icon-white.png",
            scaledSize: new google.maps.Size(25, 25)                 
          }}
      />
    </GoogleMap>
  ))
);

function GoogleMaps({ ...props }) {
  return (
    <RegularMap
      googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDJ7-qo34RvY_mdXfK-b-ImAm96S3tzUI"
      loadingElement={<div style={{ height: '100%' }} />}
      containerElement={<div style={{ height: '60vh' }} />}
      mapElement={<div style={{ height: '100%' }} />}
    />
  );
}

export default GoogleMaps;