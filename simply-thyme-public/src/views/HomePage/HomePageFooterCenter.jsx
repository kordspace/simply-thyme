import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";
import Arrow from "@material-ui/icons/ArrowDownward";

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Button from "components/CustomButtons/Button.jsx";

// sections for this page
import About from "./Sections/About.jsx";
import Consumers from "./Sections/Consumers.jsx";
import Suppliers from "./Sections/Suppliers1.jsx";
import Social from "./Sections/Social.jsx";
import Form from "./Sections/Form.jsx";

import SectionBasics from "./Sections/SectionBasics.jsx";
import SectionNavbars from "./Sections/SectionNavbars.jsx";
import SectionTabs from "./Sections/SectionTabs.jsx";
import SectionPills from "./Sections/SectionPills.jsx";
import SectionPreFooter from "./Sections/SectionPreFooter.jsx";
import SectionFooter from "./Sections/SectionFooter.jsx";
import SectionTypography from "./Sections/SectionTypography.jsx";
import SectionCards from "./Sections/SectionCards.jsx";
import SectionJavascript from "./Sections/SectionJavascript.jsx";
import SectionCarousel from "./Sections/SectionCarousel.jsx";

import homeStyle from "assets/jss/material-kit-pro-react/views/homeStyle.jsx";

import logo from "../../assets/img/eow_herologo.png"
import CardFooter from "components/Card/CardFooter.jsx";


class HomePageFooter extends React.Component {
  render() {
    const { classes } = this.props;
    const { footer } = this.props;
    if (footer) {
    return (
      <div>
        <Footer
          content={
            <div className={classes.footerContainer}>
              <div className={classes.center}>
                <List className={classes.list}>
                  <ListItem className={classes.inlineBlock}>
                    <a
                      href="/"
                      className={classes.block}
                    >
                      HOME
                    </a>
                  </ListItem>
                  <ListItem className={classes.inlineBlock}>
                    <a
                      href="#about"
                      className={classes.block}
                    >
                      ABOUT
                    </a>
                  </ListItem>
                  <ListItem className={classes.inlineBlock}>
                    <a
                      href="#footer"
                      className={classes.block}
                    >
                      CONTACT
                    </a>
                  </ListItem>
                </List>
              </div>
              <div className={classes.center}>
                <div className={classes.footerButton}>
                  <Button href={footer[0].content} target="_blank" justIcon simple color="white">
                    <i className="fab fa-twitter" />
                  </Button>
                  <Button href={footer[1].content} target="_blank" justIcon simple color="white">
                    <i className="fab fa-facebook" />
                  </Button>
                  <Button href={footer[2].content} target="_blank" justIcon simple color="white">
                    <i className="fab fa-linkedin-in" />
                  </Button>
                  <Button href={footer[3].content} target="_blank" justIcon simple color="white">
                    <i className="fab fa-instagram" />
                  </Button>
                  <Button href={footer[4].content} target="_blank" justIcon simple color="white">
                    <i className="fab fa-pinterest" />
                  </Button>
                  <Button href={footer[5].content} target="_blank" justIcon simple color="white">
                    <i className="fab fa-youtube" />
                  </Button>
                </div>
              </div>
            </div>
          }
        />
      </div>
    );
    }
  }
}

export default withStyles(homeStyle)(HomePageFooter);
