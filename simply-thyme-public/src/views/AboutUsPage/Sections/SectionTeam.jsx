import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import Button from "components/CustomButtons/Button.jsx";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

import teamStyle from "assets/jss/material-kit-pro-react/views/aboutUsSections/teamStyle.jsx";

import FaceMarcus from "assets/img/faces/marcus.jpg";
import FaceGareth from "assets/img/faces/gareth.jpg";
import FaceKendall from "assets/img/faces/kendall.jpg";
import FaceBrenden from "assets/img/faces/brenden.jpg";

function SectionTeam(props) {
  const { classes } = props;
  return (
    <div className={classes.team}>
      <GridContainer>
        <GridItem
          md={8}
          sm={8}
          className={classNames(
            classes.mrAuto,
            classes.mlAuto,
            classes.textCenter
          )}
        >
          <h1 className={classes.title}>EXECUTIVE TEAM</h1>
          <h5 className={classes.description}>
          </h5>
        </GridItem>
      </GridContainer>
      <GridContainer>
        <GridItem md={4} sm={4}>
          <Card profile plain>
            <CardAvatar profile plain>
              <a href="#pablo">
                <img
                  src={FaceMarcus}
                  alt="profile-pic"
                  className={classes.img}
                />
              </a>
            </CardAvatar>
            <CardBody plain>
              <h4 className={classes.cardTitle}>MARCUS MCNEILL</h4>
              <h5 className={classes.textMuted}>CEO</h5>
              <p className={classes.cardDescription}>
                Marcus has led marketing strategy for 50+ companies, including Eckhart Tolle, Byron Katie, Deepak Chopra, and Kim Eng. With a passion for sustainability, renewable energy, spirituality, and wellness, he aims to impact millions of lives through digital marketing in those industries. You may see him around Boulder wearing a purple top hat.
              </p>
            </CardBody>
            <CardFooter className={classes.justifyContent}>
              <Button href="#pablo" justIcon simple color="twitter">
                <i className="fab fa-twitter" />
              </Button>
              <Button href="#pablo" justIcon simple color="dribbble">
                <i className="fab fa-dribbble" />
              </Button>
              <Button href="#pablo" justIcon simple color="linkedin">
                <i className="fab fa-linkedin-in" />
              </Button>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem md={4} sm={4}>
          <Card profile plain>
            <CardAvatar profile plain>
              <a href="#pablo">
                <img
                  src={FaceGareth}
                  alt="profile-pic"
                  className={classes.img}
                />
              </a>
            </CardAvatar>
            <CardBody plain>
              <h4 className={classes.cardTitle}>GARETH HERMANN</h4>
              <h5 className={classes.textMuted}>COO</h5>
              <p className={classes.cardDescription}>
                Gareth is a systems enthusiast and team galvanizer who has managed brand strategy and leadership development projects for international corporations. His passion lies in influencing personal, organizational, and societal transformation. He’s the type of guy who is so organized he pens “unstructured time” into his schedule.
              </p>
            </CardBody>
            <CardFooter className={classes.justifyContent}>
              <Button href="#pablo" justIcon simple color="facebook">
                <i className="fab fa-facebook" />
              </Button>
              <Button href="#pablo" justIcon simple color="dribbble">
                <i className="fab fa-dribbble" />
              </Button>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem md={4} sm={4}>
          <Card profile plain>
            <CardAvatar profile plain>
              <a href="#pablo">
                <img
                  src={FaceBrenden}
                  alt="profile-pic"
                  className={classes.img}
                />
              </a>
            </CardAvatar>
            <CardBody plain>
              <h4 className={classes.cardTitle}>BRENDAN HAYES</h4>
              <h5 className={classes.textMuted}>CMO</h5>
              <p className={classes.cardDescription}>
                Brendan has quarterbacked digital marketing teams for  nationally acclaimed online retailers, boutique agencies and Inc. 500 startups. He connects the dots between analytics and emotions that drive consumer behavior. His estimated career winning percentage in rock papers scissors is over 90% (he measures everything).
              </p>
            </CardBody>
            <CardFooter className={classes.justifyContent}>
              <Button href="#pablo" justIcon simple color="google">
                <i className="fab fa-google" />
              </Button>
              <Button href="#pablo" justIcon simple color="twitter">
                <i className="fab fa-twitter" />
              </Button>
              <Button href="#pablo" justIcon simple color="dribbble">
                <i className="fab fa-dribbble" />
              </Button>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}

export default withStyles(teamStyle)(SectionTeam);
