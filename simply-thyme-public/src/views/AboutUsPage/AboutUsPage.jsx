import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx"

import aboutPageStyle from "assets/jss/material-kit-pro-react/views/aboutUsSections/aboutPageStyle.jsx";

import AIpic from "assets/img/ltr/ai-artificial-intelligence.png"
import TransPic from "assets/img/ltr/transportation-big-data.png"
import BlockPic from "assets/img/ltr/blockchain.png"
import LtrLogo from "assets/img/ltr/load-to-ride-logo.png"

import LtrIT from "assets/img/ltr/ltr-it-infrastructure-darkened.jpg"
import LtrFounding from "assets/img/ltr/ltr-founding-sd.jpg"
import LtrDev from "assets/img/ltr/ltr-development-720.jpg"
import LtrPub from "assets/img/ltr/ltr-public-offering-720.jpg"
import LtrAI from "assets/img/ltr/ltr-advanced-logistics-720.jpg"

import BDoss from "assets/img/ltr/doss720.jpg"
import ACutler from "assets/img/ltr/ACutler720.jpg"
import JBates from "assets/img/ltr/JBates720.jpg"

// Sections for this page
import NavLinks from "components/Nav/NavLinks"
import { mlAuto } from "../../assets/jss/material-kit-pro-react";

const dashboardRoutes = [];

class AboutPage extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
  }
  render() {
    const { classes, ...rest } = this.props;

    return (
      <div className={classes.wrapper}>
        <Header
          color="transparent"
          routes={dashboardRoutes}
          brand="Load To Ride"
          links={<NavLinks dropdownHoverColor="dark" />}
          fixed
          changeColorOnScroll={{
            height: 200,
            color: "dark"
          }}
          {...rest}
        />
        <Parallax image={require("assets/img/ltr/load-to-ride-about-hd.jpg")} filter="dark">
          <div className={classes.container}>
            <GridContainer
              justify="center"
            >
              <GridItem xs={12} md={8} lg={8} >
                <h1 className={classes.header1}>ABOUT US</h1>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classes.wrapper}>
          <br /><br /><br /><br /><br />
          <div className={classes.container}>
            <h1 className={classes.header2}>LOGISTICS SUPPLY CHAIN SOLUTIONS FOR LESS THAN
                TRUCKLOAD SHIPMENTS AND MORE</h1>
            <br />
            <p className={classes.bodyText1}>
              Load to Ride Transportation is a regional and national transportation leader. Operations
              under the name Load to Ride began in 2014 with only a handful of trucks, but now has expanded
              with a fleet of over 110 tractors and trailers throughout the United States and Canada.
              Corporate offices are centrally located in Denver, Colorado.</p>
            <h1 className={classes.header2}>A Cleaner Logistics Supply Chain Model</h1>
            <h4>How our proprietary technology brings efficiency and greater reliability</h4>
          </div>
          <br />
          <div
            style={{
              background: "url(" + AIpic + ")",
              backgroundSize: "cover",
              height: "60vh"
            }}
          >
            <GridContainer >
              <GridItem xs={12} md={5}
                style={{
                  marginLeft: "50px"
                }}
              >
                <br /><br /><br /><br /><br /><br />
                <h1 className={classes.header2}>ARTIFICIAL INTELLIGENCE</h1>
                <br />
                <p className={classes.bodyText1}>
                  Load to Ride is implementing artificial intelligence to increase harmonious function
                  throughout both physical and digital networks involved in logistics. AI brings us the
                  ability to optimize network orchestration to degrees of efficiency that cannot be
                  achieved with human thinking alone. Along with big data and blockchain, we are
                  employing technologies to revolutionize the logistics industry.
                </p>
              </GridItem>
            </GridContainer>
          </div>
          <div
            style={{
              background: "url(" + TransPic + ")",
              backgroundSize: "cover",
              height: "60vh"
            }}
          >
            <GridContainer justify="flex-end">
              <GridItem xs={12} md={5}
                style={{
                  marginRight: "50px"
                }}
              >
                <br /><br /><br /><br /><br /><br />
                <h1 className={classes.header2}>BIG DATA</h1>
                <br />
                <p className={classes.bodyText1}>
                  Increases in technology have allowed us to gather data streams continuosly with
                  minimal human effort. Logistics providers today manage a massive flow of goods and
                  at the same time create vast data sets. There is great potential for improving operational efficiency and customer experience, as well as optimizing resource consumption by using artificial intelligence, big data, and blockchain in the logistics industry.
                  For example, today big data solutions are helping avoid delivery delays by analysing
                  GPS data in addition to traffic and weather data to dynamically plan and optimise
                  delivery routes.
                </p>
              </GridItem>
            </GridContainer>
          </div>
          <div
            style={{
              background: "url(" + BlockPic + ")",
              backgroundSize: "cover",
              height: "60vh"
            }}
          >
            <GridContainer>
              <GridItem xs={12} md={5}
                style={{
                  marginLeft: "50px"
                }}
              >
                <br /><br /><br /><br /><br /><br />
                <h1 className={classes.header2}>BLOCKCHAIN TECHNOLOGY</h1>
                <br />
                <p className={classes.bodyText1}>
                  Blockchain technology is being employed to address common challenges in the
                  logistics industry -namely high complexity, diverse amount of stakeholders,
                  varying interests, and many third-party intermediaries. Blockchain technology
                  can also be used to increase data transparency, security, and asset management.
                </p>
              </GridItem>
            </GridContainer>
          </div>
          <br />
          <br />
          <br />
          <div
            style={{
              background: "url(" + LtrIT + ")",
              backgroundSize: "cover",
              height: "60vh"
            }}
          >
            <div className={classes.container}>
              <h1 className={classes.header1}>Headquartered Super Intelligence</h1>
              <h3 className={classes.header2}>Our Logistics Access Points & Unique Corporate Model</h3>
              <br />
              <p className={classes.bodyText1}>
                Blockchain technology is being employed to address common challenges in the
                logistics industry -namely high complexity, diverse amount of stakeholders,
                varying interests, and many third-party intermediaries. Blockchain technology
                can also be used to increase data transparency, security, and asset management.
              </p>
              <br /><br /><br /><br /><br />
            </div>
            <br /><br /><br /><br /><br />
          </div>
          <h1 className={classes.header1}>The Load to Ride Mission</h1>
          <h4 className={classes.header2}>Where Load to Ride started — and where it’s going</h4>
          <br /><br /><br />

          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <Card
                className={classes.cardStyle}
              >
                <CardBody className={classes.cardBodyStyle}>
                  <img style={{
                    height: "auto",
                    width: "100%"
                  }} src={LtrFounding} alt="LTR Founding" />
                </CardBody>
              </Card>
            </GridItem>
            <GridItem
              xs={12} sm={12} md={6}
            >
              <Card>
                <CardBody className={classes.cardBodyStyle}>
                  <h2 className={classes.header1}>FOUNDING</h2>
                  <h5 className={classes.bodyText1}>
                    Our highly specialized operations team and account-specific customer service organization
                    provides the best support to build long-lasting business relationships. From a single delivery
                    van to our truck fleet, Load to Ride provides clients with the finest, most dependable assets and
                    experts to move your freight.
                </h5>
                </CardBody>
              </Card>
            </GridItem>
            <GridItem
              xs={12} sm={12} md={6}
            >
              <Card>
                <CardBody className={classes.cardBodyStyle}>
                  <h2 className={classes.header1}>DEVELOPMENT</h2>
                  <h5 className={classes.bodyText1}>
                    Our highly specialized operations team and account-specific customer service organization
                    provides the best support to build long-lasting business relationships. From a single delivery
                    van to our truck fleet, Load to Ride provides clients with the finest, most dependable assets and
                    experts to move your freight.
                </h5>
                </CardBody>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <Card
                className={classes.cardStyle}
              >
                <CardBody className={classes.cardBodyStyle}>
                  <img style={{
                    height: "auto",
                    width: "100%"
                  }} src={LtrDev} alt="LTR Development" />
                </CardBody>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <Card
                className={classes.cardStyle}
              >
                <CardBody className={classes.cardBodyStyle}>
                  <img style={{
                    height: "auto",
                    width: "100%"
                  }} src={LtrPub} alt="LTR Public Offering" />
                </CardBody>
              </Card>
            </GridItem>
            <GridItem
              xs={12} sm={12} md={6}
            >
              <Card>
                <CardBody className={classes.cardBodyStyle}>
                  <h2 className={classes.header1}>PUBLIC OFFERING</h2>
                  <h5 className={classes.bodyText1}>
                    Our highly specialized operations team and account-specific customer service organization
                    provides the best support to build long-lasting business relationships. From a single delivery
                    van to our truck fleet, Load to Ride provides clients with the finest, most dependable assets and
                    experts to move your freight.
                </h5>
                </CardBody>
              </Card>
            </GridItem>
            <GridItem
              xs={12} sm={12} md={6}
            >
              <Card>
                <CardBody className={classes.cardBodyStyle}>
                  <h2 className={classes.header1}>ADVANCED AI LOGISTICS</h2>
                  <h5 className={classes.bodyText1}>
                    Our highly specialized operations team and account-specific customer service organization
                    provides the best support to build long-lasting business relationships. From a single delivery
                    van to our truck fleet, Load to Ride provides clients with the finest, most dependable assets and
                    experts to move your freight.
                </h5>
                </CardBody>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <Card
                className={classes.cardStyle}
              >
                <CardBody className={classes.cardBodyStyle}>
                  <img style={{
                    height: "auto",
                    width: "100%"
                  }} src={LtrAI} alt="LTR AI and Logistics" />
                </CardBody>
              </Card>
            </GridItem>
          </GridContainer>

          <h1 className={classes.header1}>Meet the Team That Made it Happen</h1>
          <br /><br />
          <GridContainer justify="center">
            <GridItem xs={12} sm={10} md={8}>
              <GridContainer>
                <GridItem xs={12} sm={4}>
                  <Card
                    className={classes.cardStyle}
                  >
                    <CardBody className={classes.cardBodyStyle}>
                      <img style={{
                        height: "auto",
                        width: "100%"
                      }} src={BDoss} alt="Bruce Doss" />
                      <h3 className={classes.header2}>BRUCE DOSS</h3>
                      <p className={classes.subtitle}>Outbound Manager</p>
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem xs={12} sm={4}>
                  <Card
                    className={classes.cardStyle}
                  >
                    <CardBody className={classes.cardBodyStyle}>
                      <img style={{
                        height: "auto",
                        width: "100%"
                      }} src={ACutler} alt="Angela Cutler" />
                      <h3 className={classes.header2}>ANGELA CUTLER</h3>
                      <p className={classes.subtitle}>Vice President</p>
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem xs={12} sm={4}>
                  <Card
                    className={classes.cardStyle}
                  >
                    <CardBody className={classes.cardBodyStyle}>
                      <img style={{
                        height: "auto",
                        width: "100%"
                      }} src={JBates} alt="Julie Bates" />
                      <h3 className={classes.header2}>JULIE BATES</h3>
                      <p className={classes.subtitle}>Logistics Director</p>
                    </CardBody>
                  </Card>
                </GridItem>                
              </GridContainer>
            </GridItem>
          </GridContainer>
          <br/><br/><br/><br/>

          <div
            className={classes.footerContainer}
          >
            <Footer
              content={
                <div>
                  <div className={classes.pullCenter}>
                    <a
                      href="/"
                      className={classes.footerBrand}
                    >
                      <img
                        src={LtrLogo}
                        alt="Load To Ride Logo"
                        style={{
                          height: "auto",
                          width: "10%"
                        }}
                      />
                    </a>
                  </div>
                  <NavLinks />
                  <br />
                  <br />
                </div>
              }
            />
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(aboutPageStyle)(AboutPage);
