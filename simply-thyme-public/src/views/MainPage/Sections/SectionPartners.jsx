import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// @material-ui/icons
import Chat from "@material-ui/icons/Chat";
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import Fingerprint from "@material-ui/icons/Fingerprint";
import Subject from "@material-ui/icons/Subject";
import WatchLater from "@material-ui/icons/WatchLater";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx"
import CardBody from "components/Card/CardBody.jsx";
import Button from "components/CustomButtons/Button.jsx";

import partnersStyle from "assets/jss/material-kit-pro-react/views/MainPage/partnersStyle.jsx";

//Logos
import coldBoxLogo from "assets/img/ltr/cold-box.png"
import keepTruckinLogo from "assets/img/ltr/keep-truckin.png"
import tmwLogo from "assets/img/ltr/tmw-truckmate.png"
import shellLogo from "assets/img/ltr/shell.png"
import dellLogo from "assets/img/ltr/dell.png"
import pondataLogo from "assets/img/ltr/pondata.png"
import xyoLogo from "assets/img/ltr/xyo.png"
import gcpLogo from "assets/img/ltr/gcp.png"



class SectionPartners extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <GridContainer justify="center">
          <GridItem xs={12} sm={10} md={10}>
            <h1 className={classes.title}>PARTNERS</h1>
          </GridItem>
        </GridContainer>
        <br />
        <br />
        <GridContainer spacing={8}>
          <GridItem
            xs={12} sm={6} md={3}
          >
            <div className={classes.wrapper}>
              <img
                src={coldBoxLogo}
                alt="Cold Box Logo"
                className={classes.img}
                style={{
                    width: "60%",
                    height: "auto"
                  }}
              />
            </div>
          </GridItem>
          <GridItem
            xs={12} sm={6} md={3}
          >
            <div className={classes.wrapper}>
              <img
                src={keepTruckinLogo}
                alt="Keep Truckin Logo"
                className={classes.img}
                style={{
                    width: "90%",
                    height: "auto"
                  }}
              />
            </div>
          </GridItem>          
          <GridItem
            xs={12} sm={6} md={3}
          >
            <div className={classes.wrapper}>
              <img
                src={tmwLogo}
                alt="TMW Logo"
                className={classes.img}
                style={{
                    width: "60%",
                    height: "auto"
                  }}
              />
            </div>
          </GridItem>
          <GridItem
            xs={12} sm={6} md={3}
          >
            <div className={classes.wrapper}>
              <img
                src={shellLogo}
                alt="Shell Logo"
                className={classes.img}
                style={{
                    width: "37%",
                    height: "auto"
                  }}
              />
            </div>
          </GridItem>          
          <GridItem
            xs={12} sm={6} md={3}
          >
            <div className={classes.wrapper}>
              <img
                src={dellLogo}
                alt="Dell Logo"
                className={classes.img}
                style={{
                    width: "70%",
                    height: "auto"
                  }}
              />
            </div>
          </GridItem>        
          <GridItem
            xs={12} sm={6} md={3}
          >
            <div className={classes.wrapper}>
              <img
                src={pondataLogo}
                alt="Pondata Logo"
                className={classes.img}
                style={{
                    width: "80%",
                    height: "auto"
                  }}
              />
            </div>
          </GridItem>          
          <GridItem
            xs={12} sm={6} md={3}
          >
            <div className={classes.wrapper}>
              <img
                src={xyoLogo}
                alt="XYO Logo"
                className={classes.img}
                style={{
                    width: "70%",
                    height: "auto"
                  }}
              />
            </div>
          </GridItem>
          <GridItem
            xs={12} sm={6} md={3}
          >
            <div className={classes.wrapper}>
              <img
                src={gcpLogo}
                alt="Google Cloud Platform Logo"
                className={classes.img}
                style={{
                    width: "50%",
                    height: "auto"
                  }}
              />
            </div>
          </GridItem>          
          
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(partnersStyle)(SectionPartners);
