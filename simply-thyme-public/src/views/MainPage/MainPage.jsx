import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Parallax from "components/Parallax/Parallax.jsx";

import mainPageStyle from "assets/jss/material-kit-pro-react/views/MainPage/mainPageStyle.jsx";

import LtrFreight from "assets/img/ltr/ltr-freight-background.jpg"
import LtrLogo from "assets/img/ltr/load-to-ride-logo.png"

// Sections for this page
import NavLinks from "components/Nav/NavLinks"
import HeaderLinks from "components/Header/HeaderLinks"
import SectionBenefits from "./Sections/SectionBenefits"
import SectionServices from "./Sections/SectionServices"
import SectionPartners from "./Sections/SectionPartners"
const dashboardRoutes = [];

class MainPage extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
  }
  render() {
    const { classes, ...rest } = this.props;

    return (
      <div>
        <Header
          color="transparent"
          routes={dashboardRoutes}
          brand="Load To Ride"
          links={<NavLinks dropdownHoverColor="dark" />}
          fixed
          changeColorOnScroll={{
            height: 200,
            color: "dark"
          }}
          {...rest}
        />
        <Parallax image={require("assets/img/ltr/load-to-ride-home-hd.jpg")} filter="dark">
          <div className={classes.container}>
            <GridContainer
              justify="center"
            >
              <GridItem xs={12} md={6} >
                <h1 className={classes.header1}>WELCOME TO LOAD TO RIDE</h1>
                <h4 className={classes.bodyText1}>
                  We are a regional and national load-to-deliver, LTL and truckload services company.
                </h4>
                <Button
                  className={classes.ltrButton}
                  size="lg"
                  href="/orders"                  
                  rel="noopener noreferrer"
                >
                  SIGN UP
                </Button>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classes.wrapper}>
          <SectionBenefits />
          <SectionServices />
          <SectionPartners />
          <div
            style={{
              background: "url(" + LtrFreight + ")",
              backgroundSize: "cover",
              height: "60vh"
            }}
          >
            <div className={classes.container}>
              <h1 className={classes.header1}>GET A QUOTE</h1>
              <br />
              <Button
                className={classes.ltrButton}
                size="lg"
                href="/estimator"                
                rel="noopener noreferrer"
              >
                PRICE ESTIMATOR
            </Button>
            </div>
          </div>
          <br />
          <br />
          <br />
          <br />
          <div
            className={classes.footerContainer}
          >
            <Footer              
              content={
                <div>
                  <div className={classes.pullCenter}>
                    <a
                      href="/"
                      className={classes.footerBrand}
                    >
                      <img
                        src={LtrLogo}
                        alt="Load To Ride Logo"
                        style={{
                          height: "auto",
                          width: "10%"
                        }}
                      />
                    </a>
                  </div>                 
                  <NavLinks />  
                  <br/>
                  <br/>                                   
                </div>
              }
            />
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(mainPageStyle)(MainPage);
