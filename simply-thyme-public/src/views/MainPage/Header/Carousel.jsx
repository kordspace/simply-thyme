import React from "react";
import { Link } from 'react-router-dom'

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
// @material-ui/icons
import Search from "@material-ui/icons/Search";
import Email from "@material-ui/icons/Email";
import Face from "@material-ui/icons/Face";
import Settings from "@material-ui/icons/Settings";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Explore from "@material-ui/icons/Explore";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Header from "components/Header/Header.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import CustomDropdown from "components/CustomDropdown/CustomDropdown.jsx";
import Button from "components/CustomButtons/Button.jsx";

import sectionsPageStyle from "assets/jss/material-kit-pro-react/views/sectionsPageStyle.jsx";

import image from "assets/img/bg.jpg";
import profileImage from "assets/img/faces/avatar.jpg";

class Carousel extends React.Component {
    render() {
        const { classes } = this.props;
        const { header } = this.props;
        const settings = {
            dots: true,
            infinite: true,
            speed: 1000,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true
          };
          console.log(classes);
        return (
            <div id="carousel">
                <div >
                    <GridContainer>
                        <GridItem xs={12} sm={12} md={12} >
                            <Carousel {...settings} className={classes.carouselHero}>
                                <div className={classes.carouselContent}>
                                    <img
                                        src={header[0].image_link}
                                        alt="First slide"
                                        className="slick-image"
                                    />
                                    <h1>{header[0].content}</h1>
                                    <h2>{header[2].content}</h2><br></br>
                                    <Button
                                        //onClick={e => {
                                            //e.preventDefault();
                                            //this.smoothScroll("about");
                                        //}}
                                        color="info"
                                        className={classes.carouselNavButton}
                                        round
                                    >
                                        {header[3].content}
                                    </Button>

                                    {/*<div className="slick-caption">
                  <h4>
                    <LocationOn className="slick-icons" />Yellowstone
                    National Park, United States
                  </h4>
</div>*/}
                                </div>
                                <div className={classes.carouselContent}>
                                    <img
                                        src={header[0].image_link}
                                        alt="First slide"
                                        className="slick-image"
                                    />
                                    <h1>{header[0].content}</h1>
                                    <h2>{header[2].content}</h2><br></br>
                                    <Button
                                        //onClick={e => {
                                            //e.preventDefault();
                                            //this.smoothScroll("about");
                                        //}}
                                        color="info"
                                        className={classes.carouselNavButton}
                                        round
                                    >
                                        {header[3].content}
                                    </Button>

                                    {/*<div className="slick-caption">
                  <h4>
                    <LocationOn className="slick-icons" />Yellowstone
                    National Park, United States
                  </h4>
</div>*/}
                                </div>
                                <div className={classes.carouselContent}>
                                    <img
                                        src={header[0].image_link}
                                        alt="First slide"
                                        className="slick-image"
                                    />
                                    <h1>{header[0].content}</h1>
                                    <h2>{header[2].content}</h2><br></br>
                                    <Button
                                        //onClick={e => {
                                            //e.preventDefault();
                                            //this.smoothScroll("about");
                                        //}}
                                        color="info"
                                        className={classes.carouselNavButton}
                                        round
                                    >
                                        {header[3].content}
                                    </Button>

                                    {/*<div className="slick-caption">
                  <h4>
                    <LocationOn className="slick-icons" />Yellowstone
                    National Park, United States
                  </h4>
</div>*/}
                                </div>
                            </Carousel>
                        </GridItem>
                    </GridContainer>
                </div>
            </div>

        );
    }
}

export default withStyles(sectionsPageStyle)(Carousel);
