import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import LinearProgress from '@material-ui/core/LinearProgress';

// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx"

import Style from "assets/jss/material-kit-pro-react/views/estimatorPageStyle.jsx";

import LtrLogo from "assets/img/ltr/load-to-ride-logo.png"

import NavLinks from "components/Nav/NavLinks"

import Resume from "assets/img/ltr/ltrResume.jpg"

// Sections for this page

import EstimatorForm from "components/Forms/EstimatorForm.jsx"

import { mlAuto } from "../../assets/jss/material-kit-pro-react";
import { greenLight } from "../../assets/jss/ltr-styles";
import { green } from "@material-ui/core/colors";

const dashboardRoutes = [];

class EstimatorPage extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
  }
  render() {
    const { classes, ...rest } = this.props;

    return (
      <div className={classes.wrapper}>
        <Header
          color="transparent"
          routes={dashboardRoutes}
          brand="Load To Ride"
          links={<NavLinks dropdownHoverColor="dark" />}
          fixed
          changeColorOnScroll={{
            height: 200,
            color: "dark"
          }}
          {...rest}
        />
        <Parallax image={require("assets/img/ltr/load-to-ride-estimator.jpg")} filter="dark">
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={12} md={8} lg={8} >
                <h1 className={classes.header1}>PRICE ESTIMATOR</h1>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classes.wrapper}>
          <br /><br /><br />
          <GridContainer justify="center">
            <GridItem xs={12} s={10} md={9}>
              <h2 className={classes.header1}>OUR PROPRIETARY TECHNOLOGY GUARANTEES LOW PRICES</h2>
              <h5 className={classes.bodyLeft}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
              nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
              nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo.</h5>
            </GridItem>
          </GridContainer>
          <br /><br /><br />
          <GridContainer justify="center">
            <GridItem xs={12} s={10} md={9}>
              <EstimatorForm />
            </GridItem>
          </GridContainer>
          <br /> <br /> <br /> <br /> <br />
          <div
            className={classes.footerContainer}
          >
            <br /><br />
            <Footer
              content={
                <div>
                  <div className={classes.pullCenter}>
                    <a
                      href="/"
                      className={classes.footerBrand}
                    >
                      <img
                        src={LtrLogo}
                        alt="Load To Ride Logo"
                        style={{
                          height: "auto",
                          width: "10%"
                        }}
                      />
                    </a>
                  </div>
                  <NavLinks />
                  <br />
                  <br />
                </div>
              }
            />
          </div>
        </div >
      </div >
    );
  }
}

export default withStyles(Style)(EstimatorPage);
