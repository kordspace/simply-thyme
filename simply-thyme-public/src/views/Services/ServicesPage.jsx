import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx"

import servicesPageStyle from "assets/jss/material-kit-pro-react/views/services/servicesPageStyle.jsx";

import LtlVolDel from "assets/img/ltr/ltlVolDel.jpg"
import ltrPool from "assets/img/ltr/ltrPool.jpg"
import ltrWarehouse from "assets/img/ltr/ltrWarehouse.jpg"
import ltrExp from "assets/img/ltr/ltrExp.jpg"
import ltrHandling from "assets/img/ltr/ltrHandling.jpg"
import ltrHazmat from "assets/img/ltr/ltrHazmat.jpg"
import ltrTemp from "assets/img/ltr/ltrTemp.jpg"

import LtrFreight from "assets/img/ltr/ltr-freight-background.jpg"

import LtrLogo from "assets/img/ltr/load-to-ride-logo.png"

// Sections for this page
import NavLinks from "components/Nav/NavLinks"
import { mlAuto } from "../../assets/jss/material-kit-pro-react";

const dashboardRoutes = [];

class ServicesPage extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
  }
  render() {
    const { classes, ...rest } = this.props;

    return (
      <div className={classes.wrapper}>
        <Header
          color="transparent"
          routes={dashboardRoutes}
          brand="Load To Ride"
          links={<NavLinks dropdownHoverColor="dark" />}
          fixed
          changeColorOnScroll={{
            height: 200,
            color: "dark"
          }}
          {...rest}
        />
        <Parallax image={require("assets/img/ltr/load-to-ride-services-hd.JPG")} filter="dark">
          <div className={classes.container}>
            <GridContainer
              justify="center"
            >
              <GridItem xs={12} md={8} lg={8} >
                <h1 className={classes.header1}>SERVICES</h1>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classes.wrapper}>
          <GridContainer justify="center">
            <GridItem xs={12} md={8} lg={8} >
              <h2 className={classes.header1}>WHAT WE DO <span className={classes.greenText}>BETTER</span></h2>
              <h4 className={classes.subtitle}>We are a logistics provider who can deliver freight to anywhere, anytime. Not only do we transport, but we also
              care. We take pride in giving our customers top-notch service and support.</h4>
              <br />
              <h4 className={classes.subtitle}>Welcome to Load to Ride. We are here to help you.</h4>
            </GridItem>
          </GridContainer>

          <br /><br />

          <GridContainer justify="center" >
            <GridItem xs={12} s={10} md={8}>
              <GridContainer justify="center" spacing={16}>
                <GridItem xs={12} sm={6}>
                  <Card
                    background
                    className={classes.cardStyles}
                    style={{
                      backgroundImage: "url(" + LtlVolDel + ")"
                    }}
                  >
                    <CardBody
                      background
                    >
                      <h3 className={classes.header3}> LOAD TO DELIVER, LTL, <br /> & VOLUME SHIPMENTS</h3>
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem
                  xs={12} sm={6}
                >
                  <Card
                    background
                    className={classes.greyBackground + ' ' + classes.cardStyles}
                  >
                    <CardBody
                      background
                    >
                      <p className={classes.bodyText1}>Load to Ride picks up at your location and delivers it
                      directly to your customer. Our unique service helps reduce loss and damage, expedites
                      shipment and improved satisfaction to your
                      customer.  <span className={classes.linkText}>Learn More</span></p>
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem xs={12} sm={6}>
                  <Card
                    background
                    className={classes.cardStyles}
                    style={{
                      backgroundImage: "url(" + ltrPool + ")"
                    }}
                  >
                    <CardBody
                      background
                    >
                      <h3 className={classes.header3}> POOL DISTRIBUTION</h3>
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem
                  xs={12} sm={6}
                >
                  <Card
                    background
                    className={classes.greyBackground + ' ' + classes.cardStyles}
                  >
                    <CardBody
                      background
                    >
                      <p className={classes.bodyText1}>Load to Ride offers secure linehaul and wide distribution of
                      freight. Our single source solution helps customers control their LTL costs, make advanced
                      appointments, minimize handling and reduce transit times throughout the United States and
                      Canada.  <span className={classes.linkText}>Learn More</span>
                      </p>
                    </CardBody>
                  </Card>
                </GridItem>

                <GridItem xs={12} sm={6}>
                  <Card
                    background
                    className={classes.cardStyles}
                    style={{
                      backgroundImage: "url(" + ltrWarehouse + ")"
                    }}
                  >
                    <CardBody
                      background
                    >
                      <h3 className={classes.header3}>WAREHOUSING</h3>
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem
                  xs={12} sm={6}
                >
                  <Card
                    background
                    className={classes.greyBackground + ' ' + classes.cardStyles}
                  >
                    <CardBody
                      background
                    >
                      <p className={classes.bodyText1}>Need a little extra room for storage? We have cost-effective
                      options to keep your products. Temperature-sensitive? No problem. Our new, modern facility
                      in Denver, Colorado provides both short-term and long-term
                      options.  <span className={classes.linkText}>Learn More</span>
                      </p>
                    </CardBody>
                  </Card>
                </GridItem>

                <GridItem xs={12} sm={6}>
                  <Card
                    background
                    className={classes.cardStyles}
                    style={{
                      backgroundImage: "url(" + ltrExp + ")"
                    }}
                  >
                    <CardBody
                      background
                    >
                      <h3 className={classes.header3}>EXPEDITED SERVICES</h3>
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem
                  xs={12} sm={6}
                >
                  <Card
                    background
                    className={classes.greyBackground + ' ' + classes.cardStyles}
                  >
                    <CardBody
                      background
                    >
                      <p className={classes.bodyText1}>From a single delivery van to our fleet, Load to Ride provides
                      our clients with the finest, most dependable assets and experts to move your freight. Extremely
                      critical shipments, same day or time definite nationwide delivery can be
                      arranged.  <span className={classes.linkText}>Learn More</span>
                      </p>
                    </CardBody>
                  </Card>
                </GridItem>

                <GridItem xs={12} sm={6}>
                  <Card
                    background
                    className={classes.cardStyles}
                    style={{
                      backgroundImage: "url(" + ltrHandling + ")"
                    }}
                  >
                    <CardBody
                      background
                    >
                      <h3 className={classes.header3}>SPECIAL HANDLING</h3>
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem
                  xs={12} sm={6}
                >
                  <Card
                    background
                    className={classes.greyBackground + ' ' + classes.cardStyles}
                  >
                    <CardBody
                      background
                    >
                      <p className={classes.bodyText1}>Load to Ride has extensive experience in moving anything.
                      For fragile freight, we offer crated and uncrated equipment in a blanket-wrapped environment.
                      We protect your cargo  ensure it is delivered on time, intact and in pristine
                      condition.  <span className={classes.linkText}>Learn More</span>
                      </p>
                    </CardBody>
                  </Card>
                </GridItem>

                <GridItem xs={12} sm={6}>
                  <Card
                    background
                    className={classes.cardStyles}
                    style={{
                      backgroundImage: "url(" + ltrHazmat + ")"
                    }}
                  >
                    <CardBody
                      background
                    >
                      <h3 className={classes.header3}>HAZMAT CERTIFIED</h3>
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem
                  xs={12} sm={6}
                >
                  <Card
                    background
                    className={classes.greyBackground + ' ' + classes.cardStyles}
                  >
                    <CardBody
                      background
                    >
                      <p className={classes.bodyText1}>Load to Ride is trained and certified to ship and transport
                      hazardous materials.  <span className={classes.linkText}>Learn More</span>
                      </p>
                    </CardBody>
                  </Card>
                </GridItem>

                <GridItem xs={12} sm={6}>
                  <Card
                    background
                    className={classes.cardStyles}
                    style={{
                      backgroundImage: "url(" + ltrTemp + ")"
                    }}
                  >
                    <CardBody
                      background
                    >
                      <h3 className={classes.header3}>TEMPERATURE PROTECTION</h3>
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem
                  xs={12} sm={6}
                >
                  <Card
                    background
                    className={classes.greyBackground + ' ' + classes.cardStyles}
                  >
                    <CardBody
                      background
                    >
                      <p className={classes.bodyText1}>Load to Ride provides a fleet of refrigerated and heat
                      equipped trailers to keep your freight moving in extreme weather
                      conditions.  <span className={classes.linkText}>Learn More</span>
                      </p>
                    </CardBody>
                  </Card>
                </GridItem>
              </GridContainer>
            </GridItem>
          </GridContainer>
          <br /><br /><br /><br />
          <div
            style={{
              background: "url(" + LtrFreight + ")",
              backgroundSize: "cover",
              height: "60vh"
            }}
          >
            <div className={classes.container}>
              <br /><br />
              <h1 className={classes.header1}>GET A QUOTE</h1>
              <br />
              <Button
                className={classes.ltrButton}
                size="lg"
                href="/estimator"                
                rel="noopener noreferrer"
              >
                PRICE ESTIMATOR
            </Button>
            </div>
          </div>
          <br /><br /><br /><br />
          <div
            className={classes.footerContainer}
          >
            <br /><br />
            <Footer
              content={
                <div>
                  <div className={classes.pullCenter}>
                    <a
                      href="/"
                      className={classes.footerBrand}
                    >
                      <img
                        src={LtrLogo}
                        alt="Load To Ride Logo"
                        style={{
                          height: "auto",
                          width: "10%"
                        }}
                      />
                    </a>
                  </div>
                  <NavLinks />
                  <br />
                  <br />
                </div>
              }
            />
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(servicesPageStyle)(ServicesPage);
