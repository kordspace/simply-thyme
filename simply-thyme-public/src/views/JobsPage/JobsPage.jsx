import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import LinearProgress from '@material-ui/core/LinearProgress';

// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx"

import Style from "assets/jss/material-kit-pro-react/views/jobsPageStyle.jsx";

import LtrLogo from "assets/img/ltr/load-to-ride-logo.png"

import NavLinks from "components/Nav/NavLinks"

import CareerMatch from "assets/img/ltr/ltr-career-match.jpg"
import Resume from "assets/img/ltr/ltrResume.jpg"

// Sections for this page

import { mlAuto } from "../../assets/jss/material-kit-pro-react";
import { greenLight } from "../../assets/jss/ltr-styles";
import { green } from "@material-ui/core/colors";

const dashboardRoutes = [];

class JobsPage extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
  }
  render() {
    const { classes, ...rest } = this.props;

    return (
      <div className={classes.wrapper}>
        <Header
          color="transparent"
          routes={dashboardRoutes}
          brand="Load To Ride"
          links={<NavLinks dropdownHoverColor="dark" />}
          fixed
          changeColorOnScroll={{
            height: 200,
            color: "dark"
          }}
          {...rest}
        />
        <Parallax image={require("assets/img/ltr/load-to-ride-jobs-hd.JPG")} filter="dark">
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={12} md={8} lg={8} >
                <h1 className={classes.header1}>JOBS</h1>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classes.wrapper}>
          <br /><br /><br />
          <GridContainer justify="center">
            <GridItem xs={12} s={10} md={8}>
              <h2 className={classes.header1}>OUR CAREER PHILOSOPHY</h2>
              <h5 className={classes.bodyLeft}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
              nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
          nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo</h5>

            </GridItem>
          </GridContainer>
          <br /><br /><br />
          <div
            style={{
              background: "url(" + CareerMatch + ")",
              backgroundSize: "cover",
              height: "60vh"
            }}
          >
            <div className={classes.container}>
              <br /><br /><br />
              <h2 className={classes.header1}>CAREER MATCH</h2>
              <br />
              <h5 className={classes.bodyText1}>Do you think you would be a good fit at Load to Ride? Take our career match quiz to find out.</h5>
              <br />
              <Button
                className={classes.ltrButton}
                size="lg"
                href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                target="_blank"
                rel="noopener noreferrer"
              >
                TAKE THE QUIZ
            </Button>
            </div>
          </div>
          <br /> <br /> <br />
          <h2 className={classes.header1}>OPEN POSITIONS</h2>
          <br /><br />
          <GridContainer justify="center">
            <GridItem xs={12} s={10} md={9}>
              <GridContainer>
                <GridItem xs={12} s={6} md={2}>
                  <p className={classes.bodyText1}>Administrative</p>
                </GridItem>
                <GridItem xs={12} s={6} md={2}>
                  <p className={classes.bodyText1}>Driving</p>
                </GridItem>
                <GridItem xs={12} s={6} md={2}>
                  <p className={classes.bodyText1}>Engineering</p>
                </GridItem>
                <GridItem xs={12} s={6} md={2}>
                  <p className={classes.bodyText1}>Logistics</p>
                </GridItem>
                <GridItem xs={12} s={6} md={2}>
                  <p className={classes.bodyText1}>Management</p>
                </GridItem>
                <GridItem xs={12} s={6} md={2}>
                  <p className={classes.bodyText1}>Technological</p>
                </GridItem>
              </GridContainer>
            </GridItem>
            <br /><br /><br />
            <GridItem xs={12} s={10} md={8}>
              <GridContainer justify="space-between">
                <GridItem xs={12} s={6} md={4}>
                  <h4 className={classes.headerLeft}>Accounting Assistant</h4>
                  <p className={classes.bodyLeft}>Denver, CO | Full Time</p>
                </GridItem>
                <GridItem xs={12} s={6} md={3}>
                  <Button
                    className={classes.ltrButton}
                    size="lg"
                    href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    APPLY NOW
                  </Button>
                </GridItem>
                <GridItem xs={12} s={12} md={12}>
                  <hr className={classes.hr} />
                </GridItem>
                <GridItem xs={12} s={6} md={4}>
                  <h4 className={classes.headerLeft}>Human Resources Administrator</h4>
                  <p className={classes.bodyLeft}>Denver, CO | Full Time</p>
                </GridItem>
                <GridItem xs={12} s={6} md={3}>
                  <Button
                    className={classes.ltrButton}
                    size="lg"
                    href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    APPLY NOW
                  </Button>
                </GridItem>
                <GridItem xs={12} s={12} md={12}>
                  <hr className={classes.hr} />
                </GridItem>
                <GridItem xs={12} s={6} md={4}>
                  <h4 className={classes.headerLeft}>Customer Service Representative</h4>
                  <p className={classes.bodyLeft}>Denver, CO | Full Time</p>
                </GridItem>
                <GridItem xs={12} s={6} md={3}>
                  <Button
                    className={classes.ltrButton}
                    size="lg"
                    href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    APPLY NOW
                  </Button>
                </GridItem>               
              </GridContainer>              
            </GridItem>
          </GridContainer>
          <br /> <br /> <br /> <br /> <br />
          <div
            style={{
              background: "url(" + Resume + ")",
              backgroundSize: "cover",
              height: "60vh"
            }}
          >
            <div className={classes.container}>
              <br /><br /><br />
              <h2 className={classes.header1}>DON’T SEE THE JOB YOU WANT?</h2>
              <br />
              <h5 className={classes.bodyText1}>That’s okay. We’re still interested. Upload a cover letter and resume below and we’ll see if we can find a fit for you.</h5>
              <br />
              <Button
                className={classes.ltrButton}
                size="lg"
                href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                target="_blank"
                rel="noopener noreferrer"
              >
                UPLOAD
            </Button>
            </div>
          </div>
          <br /> <br /> <br /> <br /> <br />
          <div
            className={classes.footerContainer}
          >
            <br /><br />
            <Footer
              content={
                <div>
                  <div className={classes.pullCenter}>
                    <a
                      href="/"
                      className={classes.footerBrand}
                    >
                      <img
                        src={LtrLogo}
                        alt="Load To Ride Logo"
                        style={{
                          height: "auto",
                          width: "10%"
                        }}
                      />
                    </a>
                  </div>
                  <NavLinks />
                  <br />
                  <br />
                </div>
              }
            />
          </div>
        </div >
      </div >
    );
  }
}

export default withStyles(Style)(JobsPage);
