import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
import { Link } from "react-router-dom";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import InfoArea from "components/InfoArea/InfoArea.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Button from "components/CustomButtons/Button.jsx";
import romania from "assets/img/romania.jpg";
import google from "assets/img/gcp.jpg";
import ethereum from "assets/img/ethereum.jpg";
import react from "assets/img/react.jpg";
import instagram from "assets/img/instagram.jpg";
import facebook from "assets/img/facebook.jpg";
import linkedin from "assets/img/linkedin.jpg";
import glass from "assets/img/glass.jpg";
import iot from "assets/img/iot.jpg";
import Subject from "@material-ui/icons/Subject";

// @material-ui icons
import Grid from "@material-ui/icons/GridOn";
import PhoneLink from "@material-ui/icons/Phonelink";
import AccessTime from "@material-ui/icons/AccessTime";
import AttachMoney from "@material-ui/icons/AttachMoney";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// images
import Vodafone from "assets/img/assets-for-demo/ourClients/vodafone.jpg";
import Microsoft from "assets/img/assets-for-demo/ourClients/microsoft.jpg";
import Harvard from "assets/img/assets-for-demo/ourClients/harvard.jpg";
import Standford from "assets/img/assets-for-demo/ourClients/stanford.jpg";
import profilePic1 from "assets/img/assets-for-demo/test1.jpg";
import profilePic2 from "assets/img/assets-for-demo/test2.jpg";
import profilePic3 from "assets/img/assets-for-demo/test3.jpg";

import overviewStyle from "assets/jss/material-kit-pro-react/views/presentationSections/overviewStyle.jsx";

class SectionOverview extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <div
          className={classes.features5}
          style={{
            backgroundImage: `url(${require("assets/img/code.jpg")})`
          }}
        >
          <GridItem
            md={8}
            className={classNames(classes.mlAuto, classes.mrAuto)}
          >
            <h2 className={classes.title}>MODERN TECHNOLOGIES</h2>
          </GridItem>
          <div className={classes.container}>
            <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Card
                      background
                      style={{ backgroundImage: `url(${google})` }}
                    >
                      <CardBody background>
                        <h6 className={classes.cardCategoryWhite}></h6>
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          <h3 className={classes.cardTitleWhite}>
                            Google Cloud Platform
                          </h3>
                        </a>
                        <p className={classes.cardDescriptionWhite}>
                          <br />
                        </p>
                        <Button round color="primary">
                          <Subject /> Button
                        </Button>
                      </CardBody>
                    </Card>
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Card
                      background
                      style={{ backgroundImage: `url(${ethereum})` }}
                    >
                      <CardBody background>
                        <h6 className={classes.cardCategoryWhite}></h6>
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          <h3 className={classes.cardTitleWhite}>
                            Ethereum
                          </h3>
                        </a>
                        <p className={classes.cardDescriptionWhite}>
                          <br />
                        </p>
                        <Button round color="primary">
                          <Subject /> Button
                        </Button>
                      </CardBody>
                    </Card>
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Card
                      background
                      style={{ backgroundImage: `url(${react})` }}
                    >
                      <CardBody background>
                        <h6 className={classes.cardCategoryWhite}></h6>
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          <h3 className={classes.cardTitleWhite}>
                            React.JS
                          </h3>
                        </a>
                        <p className={classes.cardDescriptionWhite}>
                          <br />
                        </p>
                        <Button round color="primary">
                          <Subject /> Button
                        </Button>
                      </CardBody>
                    </Card>
                  </GridItem>
                </GridContainer>
          </div>
        </div>
        <div className={classes.sectionTestimonials}>
          <div className={classes.container}>
            <GridContainer>
              <GridItem
                md={8}
                className={classNames(classes.mlAuto, classes.mrAuto)}
              >
                <h2 className={classes.description}>OUR SOCIAL MEDIA</h2>
                
                <h5>I see the impacts of what I consume/waste.</h5>
                <h5>It is imperative that we work to reverse climate change and end waste in our lifetime.</h5>
              </GridItem>
            </GridContainer>
            <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Card
                      background
                      style={{ backgroundImage: `url(${instagram})` }}
                    >
                      <CardBody background>
                        <h6 className={classes.cardCategoryWhite}></h6>
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          <h3 className={classes.cardTitleWhite}>
                            Instagram
                          </h3>
                        </a>
                        <p className={classes.cardDescriptionWhite}>
                          <br />
                        </p>
                        <Button round color="primary">
                        <i className="fab fa-instagram" /> Visit
                        </Button>
                      </CardBody>
                    </Card>
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Card
                      background
                      style={{ backgroundImage: `url(${facebook})` }}
                    >
                      <CardBody background>
                        <h6 className={classes.cardCategoryWhite}></h6>
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          <h3 className={classes.cardTitleWhite}>
                            Facebook
                          </h3>
                        </a>
                        <p className={classes.cardDescriptionWhite}>
                          <br />
                        </p>
                        <Button round color="primary">
                        <i className="fab fa-facebook" /> Visit
                        </Button>
                      </CardBody>
                    </Card>
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Card
                      background
                      style={{ backgroundImage: `url(${linkedin})` }}
                    >
                      <CardBody background>
                        <h6 className={classes.cardCategoryWhite}></h6>
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          <h3 className={classes.cardTitleWhite}>
                            LinkedIn
                          </h3>
                        </a>
                        <p className={classes.cardDescriptionWhite}>
                          <br />
                        </p>
                        <Button round color="primary">
                        <i className="fab fa-linkedin" /> Visit
                        </Button>
                      </CardBody>
                    </Card>
                  </GridItem>
                </GridContainer>

          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(overviewStyle)(SectionOverview);
