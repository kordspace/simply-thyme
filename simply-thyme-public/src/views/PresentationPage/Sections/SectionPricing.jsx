import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
import { Link } from "react-router-dom";

// core components
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
 
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import NavPills from "components/NavPills/NavPills.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import InfoArea from "components/InfoArea/InfoArea.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Phone from "@material-ui/icons/Phone";
import PinDrop from "@material-ui/icons/PinDrop";
import Check from "@material-ui/icons/Check";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

import pricingStyle from "assets/jss/material-kit-pro-react/views/presentationSections/pricingStyle.jsx";

const style = {
  ...pricingStyle
  // ...navPillsContentStyle
};

class SectionPricing extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <div className={classes.container}>
        <GridContainer>
              <GridItem
                md={8}
                className={classNames(classes.mlAuto, classes.mrAuto)}
              >
                <h1>Contact Us</h1>
                <br></br>
                <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfSwS24krlQ2FYwFItCH9ob5oJSccel3SDCUdqXu1dODuoukg/viewform?embedded=true" width="640" height="943" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
                <h4 className={classes.description}>
                </h4><br></br>
                {/*<Link to={"/contact-us"}>
                  <Button
                    color="rose"
                    target="_blank"
                    className={classes.navButton}
                    round
                  >
                    CONTACT US
                  </Button>
                  </Link>*/}

              </GridItem>
            </GridContainer>
        </div>
      </div>
    );
  }
}

export default withStyles(style)(SectionPricing);
