import { 
  main  
  } from "assets/jss/material-kit-pro-react.jsx";
import { lightGreen } from "@material-ui/core/colors";

// LTR STYLES
// Color Pallatte
const jetEngineBlack = "#1e1e1e";
const freightlinerGrey = "#282828";
const greenLight = "#7ddb46";
const snowWhite = "#f2f2f2";
const serviceBlue = "#18283f";
const unTealWeMeet = "#24b681";

const cardStyles = {
  margin: "5px",
  height: "100%",
  background: freightlinerGrey,
  backgroundSize: "cover",
  display: "flex",
  alignItems: "center",
  justifyContent: "center"
}

const wrapper = {
  ...main,
  background: "black"
}

const header1 = {
  color: snowWhite,
  justify: "center",
  textAlign: "center",
  textDecoration: "none",
  fontWeight: "500",  
  minHeight: "48px",
  fontFamily: `"Oswald", "Times New Roman", serif`
};

const header3 = {
  ...header1,
  minHeight: "21px"
}

const bodyText1 = {
  color: snowWhite,
  minHeight: "18px",
  fontWeight: "300",
  fontFamily: `"Monda", "Times New Roman", serif`
}

const bodyText2 = {
  ...bodyText1,
  minHeight: "14px"
}

const linkText = {
  ...bodyText2,
  textDecoration: "underline",
  color: greenLight,
  "&:hover,&:focus": {
    color: unTealWeMeet
  },
  "&:visited": {
    color: serviceBlue
  }
}

const ltrButton = {  
  backgroundColor: "transparent",  
  border: "2px solid "+greenLight,
  borderRadius: 0,  
  ...header3,
  fontSize: "18px",
  padding: "10px 60px",
  "&:hover,&:focus": {
    backgroundColor: greenLight
  },
  "&:active": {
    backgroundColor: unTealWeMeet,
    transition: "none" 
  }
}


export { 
  bodyText1,
  bodyText2,
  cardStyles,
  freightlinerGrey,
  greenLight,
  header1,
  header3, 
  jetEngineBlack,
  linkText,
  ltrButton,
  serviceBlue,   
  snowWhite,
  unTealWeMeet,
  wrapper
}