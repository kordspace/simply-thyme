import { 
  container,
  title,
  main,
  mainRaised 
  } from "assets/jss/material-kit-pro-react.jsx";
import { 
  header1, 
  wrapper, 
  bodyText1,
  ltrButton 
  } from "assets/jss/ltr-styles.jsx"

const sectionsPageStyle = {  
  container: {
    color: "#FFFFFF",
    ...container,
    textAlign: "center", 
    zIndex: "2"
  },
  title: {
    ...title,    
    display: "inline-block",
    position: "relative",
    marginTop: "30px",
    minHeight: "32px",
    color: "#FFFFFF",
    textDecoration: "none"
  },
  header1: {
    ...header1,    
    textAlign: "center"
  },
  subtitle: {
    fontSize: "1.313rem",
    maxWidth: "500px",
    margin: "10px auto 0"
  },
  bodyText1: {
    ...bodyText1,
    textAlign: "center"
  },
  wrapper: {
    ...wrapper   
  },
  mainRaised: {
    ...mainRaised
  },
  block: {
    color: "inherit",
    padding: "0.9375rem",
    fontWeight: "500",
    fontSize: "12px",
    textTransform: "uppercase",
    borderRadius: "3px",
    textDecoration: "none",
    position: "relative",
    display: "block"
  },
  inlineBlock: {
    display: "inline-block",
    padding: "0px",
    width: "auto"
  },
  list: {
    marginBottom: "0",
    padding: "0",
    marginTop: "0"
  },
  left: {
    float: "left!important",
    display: "block"
  },
  right: {
    padding: "15px 0",
    margin: "0",
    float: "right"
  },
  icon: {
    width: "18px",
    height: "18px",
    top: "3px",
    position: "relative"
  },  
  ltrButton: {
    ...ltrButton,
    marginTop: "10px"    
  }
};

export default sectionsPageStyle;


