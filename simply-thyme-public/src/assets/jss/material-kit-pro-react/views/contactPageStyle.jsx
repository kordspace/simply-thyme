import {
  container,
  title,
  main,
  mainRaised
} from "assets/jss/material-kit-pro-react.jsx";
import {
  cardStyles,
  freightlinerGrey,
  header1,
  jetEngineBlack,
  linkText,
  greenLight,
  wrapper,
  snowWhite,
  bodyText1,
  ltrButton
} from "assets/jss/ltr-styles.jsx"

const servicesPageStyle = {
  bodyText1,
  bodyLeft: {
    ...bodyText1,
    textAlign: "left",
    lineHeight: "1.4"
  },
  blackBody: {
    ...bodyText1,
    color: "black",
    lineHeight: 1.2,
    margin: "0 0",
    minHeight: "10px",
    padding: "0 0"
  },
  blackHeader: {
    ...header1,
    color: "black",
    fontWeight: "700",
    minHeight: "10px",
    lineHeight: 1.2,
    margin: "0 0",
    padding: "0 0"
  },
  cardStyles: {
    ...cardStyles,
    height: "80px"
  },
  cardHeader: {
    background: jetEngineBlack
  },
  cardContainer: {
    background: freightlinerGrey,
    paddingBottom: "5px"
  },
  cardTitle: {
    ...header1,
    paddingTop: "5px"
  },
  cardText: {
    ...bodyText1,
    textAlign: "center",
    padding: "15px, 30px, 10px, 30px"
  },
  container,
  greenCard: {
    ...cardStyles,
    backgroundColor: greenLight,
    borderRadius: "5px",
    height: "100%"
  },
  greyBackground: {
    backgroundColor: jetEngineBlack
  },
  image: {
    width: "100%",
    height: "auto"
  },
  linkText: {
    ...linkText,
    textAlign: "center",
    marginBottom: "30px"
  },
  linearBarColorPrimary: {
    backgroundColor: greenLight
  },
  greenText: {
    color: greenLight
  },
  cardBodyStyle: {
    backgroundColor: "black",
    textAlign: "center"
  },
  container: {
    color: "#FFFFFF",
    ...container,
    textAlign: "center",
    zIndex: 2
  },
  footerContainer: {
    backgroundColor: jetEngineBlack
  },
  title: {
    ...header1,
    textAlign: "left"
  },
  header1,
  headerLeft: {
    ...header1,
    margin: 0,
    textAlign: "left"
  },
  subtitle: {
    ...bodyText1,
    textAlign: "center"
  },
  hr: {
    borderTop: "4px solid " + greenLight
  },
  bodyText1: {
    ...bodyText1,
    textAlign: "center"
  },
  wrapper: {
    ...wrapper
  },
  mainRaised: {
    ...mainRaised
  },
  block: {
    color: "inherit",
    padding: "0.9375rem",
    fontWeight: "500",
    fontSize: "12px",
    textTransform: "uppercase",
    borderRadius: "3px",
    textDecoration: "none",
    position: "relative",
    display: "block"
  },
  inlineBlock: {
    display: "inline-block",
    padding: "0px",
    width: "auto"
  },
  list: {
    marginBottom: "0",
    padding: "0",
    marginTop: "0"
  },
  left: {
    float: "left!important",
    display: "block"
  },
  right: {
    padding: "15px 0",
    margin: "0",
    float: "right"
  },

  icon: {
    color: snowWhite,
    top: "3px",
    position: "relative"
  },
  ltrButton: {
    ...ltrButton,
    marginTop: "10px"
  }
};

export default servicesPageStyle;