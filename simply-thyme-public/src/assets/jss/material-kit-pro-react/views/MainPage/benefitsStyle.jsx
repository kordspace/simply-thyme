import { 
  bodyText1, 
  bodyText2,
  cardStyles,  
  header1 
} from "assets/jss/ltr-styles.jsx" 


const benefitsStyle = {
  section: {
    padding: "70px 0"
  },    
  title: {
    ...header1,    
    marginTop: "30px",
    marginBottom: 0,
    minHeight: "32px",
    textDecoration: "none",
    textAlign: "right"
  },
  subtitle: {
    ...bodyText1,
    marginTop: 0,
    marginBottom: "1rem",   
    minHeight: "32px",
    textDecoration: "none",
    textAlign: "right"
  },
  quote: {
    ...bodyText1,
    textAlign: "left"
  },
  cardTitle: {
    ...header1,
    textAlign: "center"
  },
  cardStyles: {    
    ...cardStyles
  },  
  description: {
    ...bodyText2,
    textAlign: "justify"    
  }   
};

export default benefitsStyle;
