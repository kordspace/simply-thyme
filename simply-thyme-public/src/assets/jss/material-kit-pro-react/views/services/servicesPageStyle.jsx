import {
  container,
  title,
  main,
  mainRaised
} from "assets/jss/material-kit-pro-react.jsx";
import {
  bodyText1,
  cardStyles,
  greenLight,
  header1,
  jetEngineBlack,
  linkText,
  ltrButton,
  wrapper
} from "assets/jss/ltr-styles.jsx"

const servicesPageStyle = {
  block: {
    color: "inherit",
    padding: "0.9375rem",
    fontWeight: "500",
    fontSize: "12px",
    textTransform: "uppercase",
    borderRadius: "3px",
    textDecoration: "none",
    position: "relative",
    display: "block"
  },
  bodyText1: {
    ...bodyText1,
    textAlign: "justify"
  },
  cardBodyStyle: {
    backgroundColor: "black",
    textAlign: "center"
  },
  cardStyles: {
    ...cardStyles,
    height: "20vh"
  },
  container: {
    color: "#FFFFFF",
    ...container,
    textAlign: "center",
    zIndex: 2
  },
  footerContainer: {
    backgroundColor: jetEngineBlack
  },
  greenText: {
    color: greenLight
  },
  greyBackground: {
    backgroundColor: jetEngineBlack
  },
  header1: {
    ...header1,
    paddingTop: "8vh"
  },
  header2: {
    ...header1,
    fontSize: "32px",
    marginBottom: 0
  },
  header3: {
    ...header1,
    fontSize: "21px"
  },
  icon: {
    width: "18px",
    height: "18px",
    top: "3px",
    position: "relative"
  },
  image: {
    width: "100%",
    height: "auto"
  },
  inlineBlock: {
    display: "inline-block",
    padding: "0px",
    width: "auto"
  },
  list: {
    marginBottom: "0",
    padding: "0",
    marginTop: "0"
  },
  left: {
    float: "left!important",
    display: "block"
  },
  ltrButton: {
    ...ltrButton,
    marginTop: "10px"
  },
  right: {
    padding: "15px 0",
    margin: "0",
    float: "right"
  },
  linkText: {
    ...linkText
  },
  mainRaised: {
    ...mainRaised
  },
  subtitle: {
    ...bodyText1,
    textAlign: "center"
  },
  title: {
    ...title,
    display: "inline-block",
    position: "relative",
    marginTop: "30px",
    minHeight: "32px",
    color: "#FFFFFF",
    textDecoration: "none"
  },
  wrapper: {
    ...wrapper
  }
};

export default servicesPageStyle;