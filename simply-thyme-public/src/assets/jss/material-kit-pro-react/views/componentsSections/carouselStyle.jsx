import { container } from "assets/jss/material-kit-pro-react.jsx";

const carouselStyle = {
  section: {
  },
  container,
  marginAuto: {
  },
  hero: {
    height: "100vh"
  },
  content: {
    "& h1": {
      marginTop: "-400px",
      color: "white",
      textAlign: "center"
    }
  },
  navButton: {
    backgroundColor: "rgba(57, 165, 133, 1)",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    width: "50%",
    zIndex: "1"
  },
};

export default carouselStyle;
